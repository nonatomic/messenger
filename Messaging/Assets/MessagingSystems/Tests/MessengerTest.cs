using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.Events;
using System.Collections;
using NUnit.Framework;
using Nonatomic.Messaging;

namespace Tests {

	public class Message : UnityEvent {}
	public class MessageOne : UnityEvent<int> {}
	public class MessageTwo : UnityEvent<string, float> {}
	public class MessageThree : UnityEvent<float, object, float> {}
	public class MessageFour : UnityEvent<int, string, float, object> {}

	[TestFixture]
	public class MessengerTest {

		private int result;

		[UnityTest]
		public IEnumerator AddListenerToMessage(){

			result = 0;

			var e = Messenger.Get<Message>();
			e.AddListener(()=>{
				result = 1;
			});
			e.Invoke();

			yield return null;

			Assert.AreEqual(result, 1, "Failed to add listener");
		}

		[UnityTest]
		public IEnumerator AddListenerToMessageOne(){

			var e = Messenger.Get<MessageOne>();
			e.AddListener((int i)=>{
				Assert.AreEqual(i, 1);
			});
			e.Invoke(1);

			yield return null;
		}

		[UnityTest]
		public IEnumerator AddListenerToMessageTwo(){

			var e = Messenger.Get<MessageTwo>();
			e.AddListener((string s, float i)=>{
				Assert.AreEqual(i, 2.1f);
				Assert.AreEqual(s, "foo");
			});
			e.Invoke("foo", 2.1f);

			yield return null;
		}

		[UnityTest]
		public IEnumerator AddListenerToMessageFour(){
			var obj = new {};
			var e = Messenger.Get<MessageFour>();
			e.AddListener((int a, string b, float c, object d)=>{
				Assert.AreEqual(a, 3);
				Assert.AreEqual(b, "bar");
				Assert.AreEqual(c, 1.1f);
				Assert.AreEqual(d, obj);
			});
			e.Invoke(3, "bar", 1.1f, obj);

			yield return null;
		}

		[Test]
		public void ReusingEvents(){

			var a = Messenger.Get<MessageTwo>();
			var b = Messenger.Get<MessageTwo>();

			Assert.AreEqual(a, b);
		}

		[UnityTest]
		public IEnumerator RemoveListener(){

			var e = Messenger.Get<MessageTwo>();
			var complete = false;
			e.AddListener((string a, float b)=>{
				complete = true;
			});	
			e.RemoveAllListeners();
			e.Invoke("foo", 1f);

			yield return null;

			Assert.IsFalse(complete, "Listener removal failed");
		}

		[Test]
		public void ClearMessenger(){

			Messenger.Get<Message>();
			Messenger.Get<MessageOne>();
			Messenger.Get<MessageOne>();
			Messenger.Clear();

			Assert.AreEqual(Messenger.EventCount(), 0);
		}

		[Test]
		public void MessageCount(){

			Messenger.Get<MessageThree>();
			Messenger.Get<MessageFour>();
			Messenger.Get<MessageOne>();

			Assert.AreEqual(Messenger.EventCount(), 3);
		}

		[Test]
		public void DuplicateMessagePrevention(){

			Messenger.Get<MessageOne>();
			Messenger.Get<MessageThree>();
			Messenger.Get<MessageThree>();
			Messenger.Get<MessageOne>();

			Assert.AreEqual(Messenger.EventCount(), 2);
		}

		[TearDown]
		public void TearDown(){

			Messenger.Clear();
		}
	}
}