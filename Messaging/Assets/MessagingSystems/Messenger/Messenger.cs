﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Nonatomic.Messaging {
	
	public class Messenger {

		private static Dictionary<Type, object> events = new Dictionary<Type, object>();

		public static T Get<T>() where T : UnityEventBase, new(){

			Type eventType = typeof(T);

			if(events.ContainsKey(eventType)){
				return (T)events[eventType];
			}
			
			var e = new T();
			events.Add(eventType, e);
			return (T)e;
		}

		public static void Clear(){
			
			foreach(KeyValuePair<Type, object> kvp in events){
				var e = (UnityEventBase)kvp.Value;
				e.RemoveAllListeners();
			}

			events.Clear();
		}

		public static int EventCount(){
			return events.Count;
		}
	}
}
