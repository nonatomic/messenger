﻿using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Messaging;

namespace Nonatomic.Examples {

	[RequireComponent(typeof(Text))]
	public class RecieveMessageWithCode : MonoBehaviour {

		private Text textField;

		private void Start(){

			textField = GetComponent<Text>();
			Messenger.Get<SendMessage>().AddListener(OnMessageRecieved);
		}

		private void OnMessageRecieved(string message){

			textField.text = message;
		}
	}

}
