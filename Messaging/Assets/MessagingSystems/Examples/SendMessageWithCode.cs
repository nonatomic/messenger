﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Nonatomic.Messaging;
using System;

namespace Nonatomic.Examples {

	[Serializable]
	public class SendMessage : UnityEvent<string> {}

	[RequireComponent(typeof(Button))]
	public class SendMessageWithCode : MonoBehaviour {

		public string message;

		public void Start(){

			var btn = GetComponent<Button>();
			btn.onClick.AddListener(OnClick);
		}

		private void OnClick(){
			
			Messenger.Get<SendMessage>().Invoke(message);
		}
	}

}