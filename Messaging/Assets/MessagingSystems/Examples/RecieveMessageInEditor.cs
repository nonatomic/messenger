﻿using UnityEngine;
using UnityEngine.UI;

namespace Nonatomic.Examples {

	[RequireComponent(typeof(Text))]
	public class RecieveMessageInEditor : MonoBehaviour {

		private Text textField;

		private void Start(){

			textField = GetComponent<Text>();
		}

		public void SetMessage(string message){

			textField.text = message;
		}
	}

}
