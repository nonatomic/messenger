﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Nonatomic.Messaging;

namespace Nonatomic.Examples {

	[RequireComponent(typeof(Button))]
	public class SendMessageInEditor : MonoBehaviour {
		

		public SendMessage sendMessage = Messenger.Get<SendMessage>();
		public string message;

		public void Start(){

			var btn = GetComponent<Button>();
			btn.onClick.AddListener(OnClick);
		}

		private void OnClick(){
			
			sendMessage.Invoke(message);
		}
	}

}